<?php

namespace Drupal\xntttsv\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\FileClientBase;

/**
 * External entities storage client for TSV files.
 *
 * @StorageClient(
 *   id = "xntttsv",
 *   label = @Translation("TSV Files"),
 *   description = @Translation("Retrieves and stores records in TSV files.")
 * )
 */
class TsvFiles extends FileClientBase {

  public const DEFAULT_FIELD_PREFIX = 'f';

  /**
   * Hash algorithm name to use.
   *
   * @var string
   */
  protected string $hashAlgo;

  /**
   * Separator string.
   *
   * @var string
   */
  protected string $separator;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    CacheBackendInterface $cache,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_type_manager,
      $entity_field_manager,
      $config_factory,
      $messenger,
      $cache
    );
    // Defaults.
    $this->fileType = 'TSV';
    $this->fileTypePlural = 'TSV';
    $this->fileTypeCap = 'TSV';
    $this->fileTypeCapPlural = 'TSV';
    $this->fileExtensions = ['.tsv', '.csv', '.gff3', '.gff', '.gtf', '.vcf'];
    $this->hashAlgo = 'crc32c';
    // Check if algo available.
    if (!in_array($this->hashAlgo, hash_algos())) {
      $this->hashAlgo = 'md4';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Adjust default File form.
    $form_override = [
      'root' => [],
      'structure' => [],
      'structure_help' => [
        'details' => [],
      ],
      'matching_only' => [],
      'record_type' => [],
      'performances' => [
        'use_index' => [],
        'generate_index' => [],
        'update_index' => [],
      ],
      'data_fields' => [
        'field_list' => [],
      ],
      'headers' => [
        '#type' => 'radios',
        '#title' => $this->t('Column headers'),
        '#options' => [
          '' => $this->t('No column headers'),
          'first' => $this->t('First (uncommented) row'),
          'comment' => $this->t('First commented row before data rows (specify a comment prefix below)'),
        ],
        '#default_value' => $this->configuration['headers'] ?? '',
      ],
      'normalize_headers' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Normalize field (header) names'),
        '#description' => $this->t(
          'If checked, all following non-word characters in column names will be replaced by an underscore and names will be lower cased. In case of numeric column names, they will be prefixed by "@colprefix".',
          ['@colprefix' => static::DEFAULT_FIELD_PREFIX]
        ),
        '#default_value' => $this->configuration['normalize_headers'] ?? '',
      ],
      'comment_prefix' => [
        '#type' => 'textfield',
        '#title' => $this->t('Ignore lines starting with (comment prefix)'),
        '#description' => $this->t('Leave empty if there are no comment lines. Ex.: "#". Note: empty lines are always ignored.'),
        '#default_value' => $this->configuration['comment_prefix'] ?? '',
      ],
      'separator' => [
        '#type' => 'textfield',
        '#title' => $this->t('Column separator'),
        '#description' => $this->t('Leave empty to use tabulation by default. Ex.: ";" for CSV files, "\t" for GFF3 or VCF files.'),
        '#default_value' => $this->configuration['separator'] ?? '',
      ],
      'quoted' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Support quoted values'),
        '#description' => $this->t('If checked, separators, new lines and 2 following double quotes found between double quotes are ignored.'),
        '#default_value' => $this->configuration['quoted'] ?? FALSE,
      ],
      'generate_id' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Use a virtual identifier for each record (_id)'),
        '#description' => $this->t('If no column could be used as an identifier, a virtual one can be generated from the record line and data. However, each time the file is modified, some identifiers may change!'),
        '#default_value' => $this->configuration['generate_id'] ?? FALSE,
      ],
    ];

    // Merge forms.
    $form = $this->overrideForm(
      $form,
      $form_override,
      [
        '#type' => 'hidden',
      ]
    );

    // Replace "record_type" options.
    $form['record_type']['#options'] = [
      static::RECORD_TYPE_MULTI => $this->t('Load TSV data.'),
      static::RECORD_TYPE_FILE => $this->t('Just gather info on TSV file and its content.'),
    ];

    return $form;
  }

  /**
   * Returns column separator string.
   *
   * @return string
   *   The column separator string.
   */
  protected function getSeparator() :string {
    if (!isset($this->separator)) {
      $this->separator = stripcslashes(
      empty($this->configuration['separator'])
        ? "\t"
        : $this->configuration['separator']
      );
    }
    return $this->separator;
  }

  /**
   * {@inheritdoc}
   */
  protected function parseFile(string $file_path) :array {
    $data = [];
    $raw_tsv = file($file_path);
    if (!empty($raw_tsv)) {
      // Remove BOM header (UTF8).
      $raw_tsv[0] = preg_replace('/^\xEF\xBB\xBF/', '', $raw_tsv[0]);
      $trash_regex = '/^\s*$/';
      // Prepare "empty lines and comment" regex.
      if (!empty($this->configuration['comment_prefix'])) {
        $comment_prefix_len = strlen($this->configuration['comment_prefix']);
        $trash_regex =
          '/^\Q'
          . $this->configuration['comment_prefix']
          . '\E|^\s*$/';
      }
      // $split_columns is a function used to split row columns.
      $sep = $this->getSeparator();
      $split_columns = function (string $s) use ($sep) {
        // Remove EOL chars.
        $s = rtrim($s, "\n\r");
        // Check quote support.
        if (empty($this->configuration['quoted'])) {
          return explode($sep, $s);
        }
        else {
          $unquoted_values = [];
          $quote_exploded = explode($sep, $s);
          $piece = array_shift($quote_exploded);
          do {
            if (!empty($piece) && ('"' === $piece[0])) {
              // Quoted value, check quote count.
              // @todo Maybe change the approach to look for string ending by
              // an odd count of '"'.
              $quote_count = substr_count($piece, '"');
              while ($quote_count % 2) {
                $next_piece = array_shift($quote_exploded);
                if (!isset($next_piece)) {
                  break;
                }
                $quote_count += substr_count($next_piece, '"');
                // Concatenate split values.
                $piece .= $sep . $next_piece;
              }
              if (substr($piece, -1) === '"') {
                // Strip quotes and unescape.
                $piece = str_replace('""', '"', substr($piece, 1, -1));
              }
              else {
                // Warn for format error.
                $this->logger->warning(
                  'File format error: invalid quotes in file "'
                  . $file_path
                  . '" (XNTT type: "'
                  . $this->externalEntityType->getDerivedEntityTypeId()
                  . "\"):\n"
                  . $piece
                );
              }
            }
            $unquoted_values[] = $piece;
            $piece = array_shift($quote_exploded);
          } while (isset($piece));
          return $unquoted_values;
        }
      };
      if ($this->getDebugLevel()) {
        $this->logger->debug(
          "TsvFile::parseFile():\nfile: @file\ntrash regex: @trash_regex\nseparator: '@sep'",
          [
            '@file' => $file_path,
            '@trash_regex' => $trash_regex,
            '@sep' => $sep,
          ]
        );
      }

      // Process header.
      $column_line = $previous_line = '';
      do {
        if (!empty($column_line)) {
          $previous_line = $column_line;
        }
        $column_line = array_shift($raw_tsv);
        // Skip comments and empty lines.
      } while (isset($column_line) && preg_match($trash_regex, $column_line));

      // Get field names.
      switch ($this->configuration['headers'] ?? '') {
        case 'comment':
          array_unshift($raw_tsv, $column_line);
          $column_line = $previous_line ?? '';
          // Remove comment prefix (if one).
          if ($comment_prefix_len) {
            $row_prefix = substr($column_line, 0, $comment_prefix_len);
            if ($row_prefix == $this->configuration['comment_prefix']) {
              $column_line = substr($column_line, $comment_prefix_len);
            }
          }

        case 'first':
          if (!empty($this->configuration['normalize_headers'])) {
            $data[''] = $split_columns($column_line);
            $columns = [];
            foreach ($data[''] as $column) {
              $columns[] = $this->normalizeColumnName($column);
            }
          }
          else {
            $data[''] = $columns = $split_columns($column_line);
          }

          break;

        default:
          // Column index will be added after.
          $data[''] = [];
          $columns = 0;
          break;
      }
      // No data, stop here.
      if (!isset($column_line)) {
        return [];
      }
      // Iterate on entity lines.
      $line_index = 0;
      $raw_line = array_shift($raw_tsv);
      // No data, stop here.
      if (!isset($raw_line)) {
        return [];
      }
      $idf = $this->getSourceIdFieldName() ?? static::DEFAULT_ID_FIELD;
      if ($this->getDebugLevel()) {
        $this->logger->debug(
          "TsvFile::parseFile(): id field used: '@idf'",
          [
            '@idf' => $idf,
          ]
        );
      }

      do {
        if (preg_match($trash_regex, $raw_line)) {
          // Skip comments and empty lines.
          $raw_line = array_shift($raw_tsv);
          continue;
        }
        // Check quote support.
        if (!empty($this->configuration['quoted'])) {
          // Check if line contains quoted line break.
          $quote_count = substr_count($raw_line, '"');
          while ($quote_count % 2) {
            $next_raw_line = array_shift($raw_tsv);
            if (!isset($next_raw_line)) {
              break;
            }
            // Concatenate broken lines.
            $quote_count += substr_count($next_raw_line, '"');
            $raw_line .= $next_raw_line;
          }
        }

        // Extract value and also remove EOL chars.
        $values = $split_columns($raw_line);
        // Check how to handle columns.
        if (is_int($columns)) {
          // We previously set $columns to 0, meaning no headers.
          if (!empty($this->configuration['normalize_headers'])) {
            // Generate normalized field names.
            $num_columns = range(0, count($values) - 1);
            foreach ($num_columns as &$column) {
              $column = $this->normalizeColumnName($column);
            }
            $entity = array_combine(
              $num_columns,
              array_values($values)
            );
          }
          else {
            $entity = $values;
          }
          $columns = max($columns, count($values));
        }
        else {
          // Make sure we don't have more values than columns.
          $values = array_pad(
            array_slice($values, 0, count($columns)), count($columns),
            NULL
          );
          $entity = array_combine($columns, $values);
        }
        // Manage generated identifiers.
        if (!empty($this->configuration['generate_id'])) {
          $_id =
            $line_index
            . '-'
            . hash($this->hashAlgo, implode('', $values));
          $entity[$idf] = $_id;
        }
        if (!empty($entity[$idf])) {
          $data[$entity[$idf]] = $entity;
        }
        else {
          $this->logger->warning(
            "Missing id field (@idf) for raw entity (@xntt_type) in file @file:\n@raw_entity",
            [
              '@idf' => $idf,
              '@file' => $file_path,
              '@xntt_type' => $this->externalEntityType->getDerivedEntityTypeId(),
              '@raw_entity' => print_r($entity, TRUE),
            ]
          );
        }
        // Process next line.
        $raw_line = array_shift($raw_tsv);
        ++$line_index;
      } while (isset($raw_line));
    }

    // Only return file statistics.
    if (static::RECORD_TYPE_FILE == $this->configuration['record_type']) {
      $info_data = $this->getFileInfo($file_path);
      $id = key($info_data);
      $info_data[$id]['id'] = $id;
      $info_data[$id]['record_count'] = count($data) - 1;
      $info_data[$id]['column_count'] = is_int($columns) ? $columns : count($columns);
      $info_data[$id]['columns'] = is_int($columns) ? [] : $columns;
      $data = $info_data;
    }

    return $data;
  }

  /**
   * Normalize a column name.
   *
   * @param string|int $column_name
   *   Original column name.
   *
   * @return string
   *   Normalized name.
   */
  protected function normalizeColumnName(string|int $column_name) :string {
    if (is_int($column_name)) {
      return static::DEFAULT_FIELD_PREFIX . $column_name;
    }
    return strtolower(trim(preg_replace('#\W+#', '_', $column_name), '_'));
  }

  /**
   * {@inheritdoc}
   */
  protected function generateRawData(array $entities_data) :string {
    $raw_data = '';
    // Ignore columns (handled by generateFileHeader()).
    $columns = $entities_data[''];
    if (!empty($this->configuration['normalize_headers'])) {
      foreach ($columns as &$column) {
        $column = $this->normalizeColumnName($column);
      }
    }
    unset($entities_data['']);
    // Write records.
    foreach ($entities_data as $id => $entity) {
      $entity_data = [];
      foreach ($columns as $field) {
        $value = str_replace(
          ["\\", "\n", "\t", "\r"],
          ['\\\\', '\n', '\t', '\r'],
          $entity[$field] ?? ''
        );
        if ($this->configuration['quoted'] && (FALSE !== strpos($value, '"'))) {
          $value = '"' . str_replace('"', '""', $value) . '"';
        }
        $entity_data[] = $value;
      }
      $raw_data .= implode("\t", $entity_data) . "\n";
    }
    return $raw_data;
  }

  /**
   * Generates file header.
   *
   * @param string $file_path
   *   Path to the original file.
   * @param array $entities_data
   *   An array of entity data.
   *
   * @return string
   *   The file header.
   */
  protected function generateFileHeader(
    string $file_path,
    array $entities_data,
  ) :string {
    $header = '';
    $raw_tsv = file($file_path);
    if (!empty($raw_tsv)) {
      $trash_regex = '/^\s*$/';
      // Prepare comment regex.
      // Use a regex impossible to match by default.
      $comment_regex = '/\n\n/';
      if (!empty($this->configuration['comment_prefix'])) {
        $comment_prefix_len = strlen($this->configuration['comment_prefix']);
        $comment_regex =
          '/^\Q'
          . $this->configuration['comment_prefix']
          . '\E/';
      }

      // Get and keep existing header.
      $line = '';
      $is_comment = FALSE;
      while (isset($line) && ($is_comment || preg_match($trash_regex, $line))) {
        $is_comment = preg_match($comment_regex, $line);
        if ($is_comment) {
          $header .= $line;
        }
        $line = array_shift($raw_tsv);
      }

      // Add field names.
      switch ($this->configuration['headers'] ?? '') {
        case 'first':
          // Add columns.
          $header .=
            implode(
              $this->getSeparator(),
              $entities_data['']
            )
            . "\n";
          break;

        default:
          break;
      }
    }
    return $header;
  }

  /**
   * Generates file footer.
   *
   * @param string $file_path
   *   Path to the original file.
   * @param array $entities_data
   *   An array of entity data.
   *
   * @return string
   *   The file footer.
   */
  protected function generateFileFooter(
    string $file_path,
    array $entities_data,
  ) :string {
    $footer = '';
    $raw_tsv = file($file_path);
    if (!empty($raw_tsv)) {
      $trash_regex = '/^\s*$/';
      // Prepare comment regex.
      // Use a regex impossible to match by default.
      $comment_regex = '/\n\n/';
      if (!empty($this->configuration['comment_prefix'])) {
        $comment_prefix_len = strlen($this->configuration['comment_prefix']);
        $comment_regex =
          '/^\Q'
          . $this->configuration['comment_prefix']
          . '\E/';
      }

      // Skip existing header.
      $line = '';
      while (isset($line)
          && (preg_match($comment_regex, $line)
              || preg_match($trash_regex, $line))
      ) {
        $line = array_shift($raw_tsv);
      }

      // Collect all comments after the header and place them in the footer.
      while (isset($line)) {
        if (preg_match($comment_regex, $line)) {
          $footer .= $line;
        }
        $line = array_shift($raw_tsv);
      }

    }
    return $footer;
  }

}
